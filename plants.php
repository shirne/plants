<?php
/**
 * 保存数据为Excel格式
 * 可用参数:
 *  1->语言  ch/en
 *  2->分割文件行数 默认2000行一个文件,需要按20的整数倍计算
 */
ini_set('memory_limit', '2496M');

require('common.php');

//excel库
require('phpexcel/excel.php');

$filerow=2000;

if(isset($argv[1]) && !empty($argv[1])){
    $lang=trim($argv[1]);
}
if(isset($argv[2]) && !empty($argv[2])){
    $filerow=intval($argv[2]);
}

//excel文件名
$idx=0;
$excel_file=__DIR__.DS.$lang.DS.'plants.'.$idx.'.xls';
@mkdir(dirname($excel_file),0777,TRUE);
$excel=new Excel();



$ch = curl_init();

//curl_setopt( $ch, CURLOPT_HTTP_VERSION , CURL_HTTP_VERSION_1_1 );
curl_setopt( $ch, CURLOPT_HEADER, 1);
curl_setopt( $ch, CURLOPT_USERAGENT , 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)' );
curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT , 60 );
curl_setopt( $ch, CURLOPT_TIMEOUT , 60);
curl_setopt( $ch, CURLOPT_RETURNTRANSFER , true );
curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true); 

curl_setopt( $ch, CURLOPT_COOKIESESSION, TRUE); 
//curl_setopt( $ch, CURLOPT_COOKIEFILE, $cookie);
//curl_setopt( $ch, CURLOPT_COOKIEJAR, $cookie);
//curl_setopt( $ch, CURLOPT_COOKIE, session_name() . '=' . session_id()); 
//curl_setopt( $ch, CURLOPT_COOKIE, GetCookies($cookie));

curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE);

curl_setopt( $ch, CURLOPT_URL , $rooturl);
$data=curl_exec($ch);
$data=ihttp_response_parse($data);


//授权
checkauth();

//en_us zh-CN zh
$idindex=getdata($rooturl.$url_index);

$headers=false;


if(!empty($idindex['found'])){
	//获取已导入的
	$loaded=array();
	$loadlog=__DIR__.DS.$lang.'-loaded.txt';
	if(file_exists($loadlog)){
		$ids=file_get_contents($loadlog);
		$loaded=explode(',', $ids);
	}

    $request=array();
    if($excel->read($excel_file)){
		$headers=$excel->getHeader();
	}
    sort($idindex['found']);
    foreach($idindex['found'] as $id){
    	if(in_array($id,$loaded))continue;
        $request[]=$id;
        //批量获取
        if(count($request)>19){
            checkauth();
            $datas=getdata($rooturl.$url_list.implode(',', $request));
            if(!empty($datas) && !empty($datas['plants'])){
            	if(checkfilesize()){
            		$headers=array();
            	}
            	
                foreach ($datas['plants'] as $plant) {
                    if(empty($headers)){
                        $i=1;
                        foreach ($plant as $key => $value) {
                            $headers[$key]=$i++;
                        }
                        $excel->setHeader(array_keys($headers));
                    }

                    $row=array();
                    foreach ($plant as $key => $value) {
                        if($key=='attributes'){
                            $attributes=array();
                            foreach ($value as $k=>$attr) {
                                $attributes[]=$k.':'.implode(',',$attr);
                            }
                            $row[$headers['attributes']]=implode(';', $attributes);
                        }elseif($key=='common_names'){
                            $names=array();
                            foreach ($value as $name) {
                                $names[]=$name['common_name'];
                            }
                            $row[$headers['common_names']]=implode(',', $names);
                        }elseif($key=='images'){
                            $images=array();
                            //$dir=__DIR__.DS.'photo'.DS.($plant['id']%100).DS.$plant['id'].DS;
                            //@mkdir($dir,0777,TRUE);
                            foreach ($value as $image) {
                                $filename=file_name($image['image_path']);
                                $images[]=$filename;
                                //$localfile=$dir.$filename;
                                //if(!file_exists($localfile) || filesize($localfile)<100){
                                    //file_put_contents($localfile, file_get_contents($image['image_path']));
                                //}
                            }
                            $row[$headers['images']]=implode(',', $images);
                        }else{
                            $row[$headers[$key]]=$value;
                        }
                    }
                    $excel->addRow($row);
                }

                appendLog($loadlog,",".implode(',',$request));
                echo str_repeat("\x08", $rownum);
            	echo "Loaded:".implode(',', $request)."\n";
            	$rownum=$excel->getCurrentRow();
            	echo $rownum;
            }
            sleep(1);
            $request=array();
        }
    }
    //最后保存
    $excel->saveTo($excel_file);
}

curl_close( $ch );
