<?php
require_once __DIR__ . '/PHPExcel.php';

if(!defined('DATAPATH'))define('DATAPATH', './');
if(!defined('DS'))define('DS', DIRECTORY_SEPARATOR);

final class Excel {

	/**
	 * Excel5,Excel2007
	 */
	private $format;

	private $excel;
	private $sheet;
	private $rownum;

	private $columntype;
	private $columnmap;
	private $headermap;

    /**
     * @param string $fmt Excel5,Excel2007
     */
	function __construct($fmt='Excel5'){
		$this->format=$fmt;
		$this->excel=new PHPExcel();
		$this->sheet=$this->excel->getActiveSheet();
		$this->rownum=1;
		$this->columnmap=array();
		$this->headermap=array();
		$words='ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$l=strlen($words);
		for ($i=0; $i < $l; $i++) { 
			$this->columnmap[]=substr($words, $i, 1);
		}
		for ($i=0; $i < $l; $i++) {
			for ($j=0; $j < $l; $j++) {
				$this->columnmap[]=$this->columnmap[$i].$this->columnmap[$j];
			}
		}
		$this->columntype=array();
	}

	function getFormat(){
		return $this->format;
	}

	function read($path){
		$this->columntype=array();
		$this->rownum=1;
		$this->headermap=array();
		if(empty($path) || !is_file($path)){
			$this->excel=new PHPExcel();
			$this->sheet=$this->excel->getActiveSheet();
			return false;
		}
		if($this->format=='Excel2007'){
			$PHPReader = new PHPExcel_Reader_Excel2007();
		}else{
			$PHPReader = new PHPExcel_Reader_Excel5();
		}
		if($PHPReader->canRead($path)){
			$this->excel= $PHPReader->load($path);
			$this->sheet=$this->excel->getActiveSheet();
			$this->rownum=$this->sheet->getHighestRow()+1;
			if($this->rownum>1){
				$colname=$this->sheet->getHighestColumn();
				foreach ($this->columnmap as $key => $value) {
					if($colname==$value){
						$cols=$key+1;
						break;
					}
				}
				if(!empty($cols)){
					for($i=0;$i<$cols;$i++) {
						$val=$this->sheet->getCell($this->columnmap[$i].'1')->getValue();
						$this->headermap[$val]=$i;
					}
				}
			}
			return true;
		}
		$this->excel=new PHPExcel();
		$this->sheet=$this->excel->getActiveSheet();
		return false;
	}

	function getHeader(){
		return $this->headermap;
	}

	function getCurrentRow(){
		return $this->rownum;
	}

	function getRowCount(){
		return $this->sheet->getHighestRow();
	}

	function getColIndex($key){
		return isset($this->headermap[$key])?$this->headermap[$key]:FALSE;
	}

	function getRow($num){
		$return=array();
		foreach ($this->headermap as $key => $value) {
			$return[$key]=$this->sheet->getCell($this->columnmap[$value].$row)->getValue();
		}
		return $return;
	}

	function getCell($row,$col){
		$colw=isset($this->columnmap[$col])?$this->columnmap[$col]:$col;
		return $this->sheet->getCell($colw.$row)->getValue();
	}

	function getExcel(){
		return $this->excel;
	}

	function getSheet(){
		return $this->sheet;
	}

	function setInfo($info){
		$prop=$this->excel->getProperties();
		if(isset($info['creator']))$prop->setCreator($info['creator']);
		if(isset($info['modified_by']))$prop->setLastModifiedBy($info['modified_by']);
		if(isset($info['title']))$prop->setTitle($info['title']);
		if(isset($info['subject']))$prop->setSubject($info['subject']);
		if(isset($info['description']))$prop->setDescription($info['description']);
		if(isset($info['keywords']))$prop->setKeywords($info['keywords']);
		if(isset($info['category']))$prop->setCategory($info['category']);
	}

    /**
     * @param $column int|string
     * @param $type string PHPExcel_Cell_DataType
     */
	function setColumnType($column,$type){
		if(is_numeric($column)){
			$column=$this->columnmap[$column];
		}
		$this->columntype[$column]=$type;
	}

	function setTitle($title){
		$this->sheet->setTitle($title);
	}

	function setPageHeader($header='&C&B&TITLE'){
		$this->sheet->getHeaderFooter()->setOddHeader(str_replace('&TITLE',$this->excel->getProperties()->getTitle(),$header));

	}

	function setPageFooter($footer='&L&TITLE &RPage &P of &N'){
		$this->sheet->getHeaderFooter()->setOddFooter(str_replace('&TITLE',$this->excel->getProperties()->getTitle(),$footer));
	}

	/**
	 * 设置表头,其实与设置行一样，以后会增加其它功能
	 */
	function setHeader($header=array()){
		$this->headermap=array();
		$i=0;
		foreach ($header as $key => $value) {
			$this->sheet->setCellValueExplicit($this->columnmap[$i].$this->rownum,$value);
			$this->headermap[$value]=$i;
			$i++;
		}
		$this->rownum++;
	}

	function addRow($row){
		$i=0;
		foreach ($row as $key => $value) {
			if(isset($this->columntype[$this->columnmap[$i]])){
				$this->sheet->setCellValueExplicit($this->columnmap[$i].$this->rownum,$value,$this->columntype[$this->columnmap[$i]]);
			}else{
				$this->sheet->setCellValue($this->columnmap[$i].$this->rownum,$value);
			}
			$i++;
		}
		$this->rownum++;
	}

	/**
	 * 删除行,默认删除尾行
	 */
	function delRow($row=null, $num=1){
		if(is_null($row)){
			$row=$this->rownum;
		}
		$this->sheet->removeRow($row,$num);
		$this->rownum -= $num;
	}

	/**
	 * 清空,是否清空表头
	 */
	function clear($wh=false){
		$this->sheet->removeRow($wh?1:2,$this->rownum);
		$this->rownum=$wh?1:2;
	}

	/**
	 * 保存到文件
	 * @return TRUE/FALSE
	 */
	function saveTo($path){
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, $this->format);
		$objWriter->save($path);
	}

	/**
	 * 输出
	 * @param 文件名
	 */
	function output($filename=''){
        $dir=DATAPATH.DS.'tmp'.DS.'excel';
		if(!is_dir($dir)){
			mkdir($dir,0777,TRUE);
		}
		$path=$dir.DS.time().'-'.base_convert(microtime(), 10, 32).'.tmp';
		$this->saveTo($path);
		$file = fopen($path,"r"); // 打开文件
		$size=filesize($path);
		// 输入文件标签
		Header("Content-type: application/vnd.ms-excel");
		Header("Accept-Ranges: bytes");
		Header("Accept-Length: ".$size);
		Header("Content-Disposition: attachment; filename=" . $filename.'.'.($this->format=='Excel2007'?'xlsx':'xls'));
		// 输出文件内容
		echo fread($file,$size);
		fclose($file);
		unlink($path);
		exit();
	}
}