<?php
/**
 * 保存图片
 * 可用参数:
 *  1->启用任务数
 *  2->当前任务号
 */

require('common.php');

//多任务分配
$mod=isset($argv[1])?intval($argv[1]):0;
$mod_i=isset($argv[2])?intval($argv[2]):0;


$ch = curl_init();

//curl_setopt( $ch, CURLOPT_HTTP_VERSION , CURL_HTTP_VERSION_1_1 );
curl_setopt( $ch, CURLOPT_HEADER, 1);
curl_setopt( $ch, CURLOPT_USERAGENT , 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)' );
curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT , 60 );
curl_setopt( $ch, CURLOPT_TIMEOUT , 60);
curl_setopt( $ch, CURLOPT_RETURNTRANSFER , true );
curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true); 

curl_setopt( $ch, CURLOPT_COOKIESESSION, TRUE); 
//curl_setopt( $ch, CURLOPT_COOKIEFILE, $cookie);
//curl_setopt( $ch, CURLOPT_COOKIEJAR, $cookie);
//curl_setopt( $ch, CURLOPT_COOKIE, session_name() . '=' . session_id()); 
//curl_setopt( $ch, CURLOPT_COOKIE, GetCookies($cookie));

curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE);

curl_setopt( $ch, CURLOPT_URL , $rooturl);
$data=curl_exec($ch);
$data=ihttp_response_parse($data);


//授权
checkauth();

//en_us zh-CN zh
$idindex=getdata($rooturl.$url_index);

$headers=false;


if(!empty($idindex['found'])){
	//获取已导入的
	$loaded=array();
	$loadlog=__DIR__.DS.'loadedimg.txt';
	if(file_exists($loadlog)){
		$ids=file_get_contents($loadlog);
		$loaded=explode(',', $ids);
	}

    $request=array();
    sort($idindex['found']);
    foreach($idindex['found'] as $id){
    	//已完成任务
    	if(in_array($id,$loaded))continue;

    	//多任务分配
    	if($mod>0)if($id % $mod != $mod_i)continue;

        $request[]=$id;
        //批量获取
        if(count($request)>19){
            checkauth();
            $datas=getdata($rooturl.$url_list.implode(',', $request));
            if(!empty($datas) && !empty($datas['plants'])){
            	echo "Loading:".implode(',', $request)."\n";
            	$imgnum=0;
                $time=time();
                $lineinfo=$imgnum.' at '.date('H:i:s');
                $loadpre="Loaded images: ";
            	echo $loadpre.$lineinfo;
                foreach ($datas['plants'] as $plant) {
                    $row=array();
                    foreach ($plant as $key => $value) {
                        if($key=='attributes'){
                            $attributes=array();
                            foreach ($value as $k=>$attr) {
                                $attributes[]=$k.':'.implode(',',$attr);
                            }
                            $row[$headers['attributes']]=implode(';', $attributes);
                        }elseif($key=='common_names'){
                            $names=array();
                            foreach ($value as $name) {
                                $names[]=$name['common_name'];
                            }
                            $row[$headers['common_names']]=implode(',', $names);
                        }elseif($key=='images'){
                            $images=array();
                            $dir=__DIR__.DS.'photo'.DS.($plant['id']%100).DS.$plant['id'].DS;
                            if(!file_exists($dir))@mkdir($dir,0777,TRUE);
                            foreach ($value as $image) {
                                $filename=file_name($image['image_path']);
                                $images[]=$filename;
                                $localfile=$dir.$filename;
                                if(!file_exists($localfile) || filesize($localfile)<100){
                                	if($filedata=getfile($image['image_path'])){
	                                    file_put_contents($localfile, $filedata);
	                                    echo str_repeat("\x08", strlen($lineinfo));
	                                    $imgnum++;
                                        $lineinfo=$imgnum.' at '.date('H:i:s');
	                                    echo $lineinfo;
	                                }else{
	                                	echo str_repeat("\x08", strlen($loadpre.$lineinfo));
	                                	echo "Load error:".$image['image_path']."\n";
                                        $lineinfo=$imgnum.' at '.date('H:i:s');
	                                	echo $loadpre.$lineinfo;
	                                }
                                }
                            }
                            $row[$headers['images']]=implode(',', $images);
                        }else{
                            $row[$headers[$key]]=$value;
                        }
                    }
                }
                echo str_repeat("\x08", strlen($lineinfo));
                $seconds=time()-$time;
                $m=intval($seconds/60);
                $s=$seconds % 60;
                echo str_pad($imgnum." in ".($m>0?($m.'m'):'').(($s>0 || $m==0)?($s.'s'):''),strlen($lineinfo))."\n";

            	appendLog($loadlog,",".implode(',',$request));
            }
            
            sleep(1);
            $request=array();
        }
    }
}

curl_close( $ch );
