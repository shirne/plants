<?php
/**
 * 配置及通用函数库
 * Author:Shirne
 *
 */

define('DS', DIRECTORY_SEPARATOR);
define('DEBUGGING', 1);

/**
 * en fr de es nl it jp zh
 * en_us fr de es en_us it ja zh
 */
$lang='en';

$domain='myflowerpower.parrot.com';
$rooturl='https://myflowerpower.parrot.com/';

//授权网址
$url_auth='user/v1/authenticate';
$auther=array(
    'grant_type'=>'client_credentials',
    'client_id'=>'web_access_001',
    'client_secret'=>'6c2a6a924c385e72c6bb824c3a98c3d28719cfcf7b66'
);
$authed=array();

//获取索引的网址
$url_index='search/v5/plants/?generate_index=ASC';
//获取列表的网址
$url_list='plant_library/v1/plants/';
//获取详情的网址
$url_detail='plant_library/v2/plant/';




/**
 * 写入日志文件内容,追加模式
 * @param $file string 完整日志文件路径
 * @param $logstr string 要加的日志内容
 */
function appendLog($file,$logstr){
    $logf=fopen($file, 'a');
    fwrite($logf, ',');
    fwrite($logf, $logstr);
    fclose($logf);
}

/**
 * 检查Excel文件数据行数是否达到设定，达到则生成新文件
 * @return bool
 */
function checkfilesize(){
    global $excel_file,$idx,$excel,$rownum,$filerow;
    
    if($rownum>$filerow){
        
        $excel->saveTo($excel_file);
        $excel=new Excel($excel->getFormat());

        $excel_file=str_replace('.'.$idx.'.xls', '.xls', $excel_file);
        $idx++;
        $excel_file=str_replace('.xls', '.'.$idx.'.xls', $excel_file);
        echo "\n";
        echo "New file:".$excel_file."\n";
        return true;
    }else{
        //echo $excel->getCurrentRow()."\n";
    }
    return false;
}

/**
 * 检查授权是否过期并重新获取授权
 */
function checkauth(){
    global $authed,$ch,$rooturl,$url_auth,$auther;
    if(empty($authed['access_token']) || time()-60>$authed['start']+$authed['expires_in']){
        $authed['start']=time();
        curl_setopt( $ch , CURLOPT_URL , $rooturl.$url_auth.'?'.getstr($auther));
        $data=curl_exec($ch);
        $data=ihttp_response_parse($data);
        $json=json_decode($data['content'],TRUE);
        if(empty($json)){
            echo 'Error: Auth failed '.$data['content'];
            sleep(2);
            checkauth();
        }
        $authed['access_token']=$json['access_token'];
        $authed['expires_in']=$json['expires_in'];
        echo "Auth: ".$authed['access_token']."\n";
    }
}

/**
 * 获取从网址json数据
 * @param $url string
 * @param int $times 当前重复请求次数(出错时自动重新请求)，内部使用
 * @return array|mixed
 */
function getdata($url,$times=0){
    global $ch,$authed,$lang;

    curl_setopt( $ch, CURLOPT_COOKIE, GetCookies(array('userLang'=>$lang,'guest_access_token'=>$authed['access_token'])));
    curl_setopt( $ch, CURLOPT_HTTPHEADER , array('HTTP_ACCEPT_LANGUAGE: '.$lang,"Authorization: Bearer ".$authed['access_token']));
    curl_setopt( $ch, CURLOPT_URL , $url);
    $data=curl_exec($ch);
    $data=ihttp_response_parse($data);
    if(!empty($data['content']))$json=json_decode($data['content'],TRUE);
    if(empty($json)){
        echo 'Warning: '.$url.' '.(isset($data['content'])?$data['content']:'')."\n";
        if($times>4){
            return array();
        }
        sleep(2);
        return getdata($url,$times+1);
    }
    if($times>0){
        echo 'Success: '.$url." \n";
    }
    return $json;
}

/**
 * 下载文件
 * @param $file string
 * @param int $times 当前重复请求次数(出错时自动重新请求)，内部使用
 * @return bool|string False 获取失败/string 文件数据
 */
function getfile($file,$times=0){
    $data=@file_get_contents($file);
    if(strlen($data)<100){
        //echo "Load error:".$file."\n";
        if($times>3){
            return FALSE;
        }
        sleep(1);
        return getfile($file,$times+1);
    }
    if($times>0){
        //echo "Load success:".$file."\n";
    }
    return $data;
}

if(!function_exists('array_column')){
    function array_column($array , $column, $key='' ){
        $return=array();
        if(empty($key)){
            foreach ($array as $key => $value) {
                $return[]=@$value[$column];
            }
        }else{
            foreach ($array as $key => $value) {
                $return[$value[$key]]=@$value[$column];
            }
        }
        return $return;
    }
}

/**
 * 根据网址或路径获取末尾的文件名部分
 * @param $file string
 * @return string
 */
function file_name($file){
    $lpos=strrpos(str_replace('\\', '/', $file), '/');
    return substr($file,$lpos+1);
}

/**
 * 将数组联结成请求字符串
 * @param $arr array
 * @return string
 */
function getstr($arr){
    $paramstr=array();
    foreach($arr as $key=>$val){
        if(is_array($val)){
            foreach($val as $sval){
                $paramstr[]=$key.'='.urlencode($sval);
            }
        }else{
            $paramstr[]=$key.'='.urlencode($val);
        }
    }
    return implode('&',$paramstr);
}

/**
 * 将数组生成cookie格式字符串
 * @param $cookie string
 * @return string
 */
function GetCookies($cookie){
    $cookiestr=array();
    foreach ($cookie as $key => $value) {
        $cookiestr[]=urlencode($key).'='.urlencode($value);
    }
    return implode('; ', $cookiestr);
}

/**
 * 将cookie字符串解析成数组
 * @param $headers array 请求返回数据的headers
 * @return array
 */
function SetCookies($headers){
    $cks=explode('; ', $headers['Set-Cookie']);
    $cookiearr=array();
    foreach ($cks as $value) {
        $dpos=strpos($value,'=');
        $key=trim(substr($value,0,$dpos));
        $value=trim(substr($value,$dpos));
        if(isset($cookiearr[$key])){
            if(!is_array($cookiearr[$key])){
                $cookiearr[$key]=array($cookiearr[$key]);
            }
            $cookiearr[$key][]=$value;
        }else {
            $cookiearr[$key] = $value;
        }
    }
    return $cookiearr;
}

/**
 * 解析curl返回的数据(带header)
 * @param $data string 返回的完整数据
 * @param bool|false $chunked
 * @return array
 */
function ihttp_response_parse($data, $chunked = false) {
    $rlt = array();
    if(empty($data))return $rlt;
    $pos = strpos($data, "\r\n\r\n");
    $split1[0] = substr($data, 0, $pos);
    $split1[1] = substr($data, $pos + 4, strlen($data));
    
    $split2 = explode("\r\n", $split1[0], 2);
    preg_match('/^(\S+) (\S+) (\S+)$/', $split2[0], $matches);
    $rlt['code'] = $matches[2];
    $rlt['status'] = $matches[3];
    $rlt['responseline'] = $split2[0];
    $header = explode("\r\n", $split2[1]);
    $isgzip = false;
    $ischunk = false;
    foreach ($header as $v) {
        $row = explode(':', $v);
        $key = trim($row[0]);
        $value = trim($row[1]);
        if (isset($rlt['headers'][$key])) {
            if (is_array($rlt['headers'][$key])) {
                $rlt['headers'][$key][] = $value;
            } else{
                $temp = $rlt['headers'][$key];
                unset($rlt['headers'][$key]);
                $rlt['headers'][$key][] = $temp;
                $rlt['headers'][$key][] = $value;
            }
        } else {
            $rlt['headers'][$key] = $value;
        }
        if(!$isgzip && strtolower($key) == 'content-encoding' && strtolower($value) == 'gzip') {
            $isgzip = true;
        }
        if(!$ischunk && strtolower($key) == 'transfer-encoding' && strtolower($value) == 'chunked') {
            $ischunk = true;
        }
    }
    if($chunked && $ischunk) {
        $rlt['content'] = ihttp_response_parse_unchunk($split1[1]);
    } else {
        $rlt['content'] = $split1[1];
    }
    if($isgzip && function_exists('gzdecode')) {
        $rlt['content'] = gzdecode($rlt['content']);
    }

    $rlt['meta'] = $data;
    if($rlt['code'] == '100') {
        return ihttp_response_parse($rlt['content']);
    }
    return $rlt;
}

/**
 * @param null $str
 * @return bool|null|string
 */
function ihttp_response_parse_unchunk($str = null) {
    if(!is_string($str) or strlen($str) < 1) {
        return false; 
    }
    $eol = "\r\n";
    $add = strlen($eol);
    $tmp = $str;
    $str = '';
    do {
        $tmp = ltrim($tmp);
        $pos = strpos($tmp, $eol);
        if($pos === false) {
            return false;
        }
        $len = hexdec(substr($tmp, 0, $pos));
        if(!is_numeric($len) or $len < 0) {
            return false;
        }
        $str .= substr($tmp, ($pos + $add), $len);
        $tmp  = substr($tmp, ($len + $pos + $add));
        $check = trim($tmp);
    } while(!empty($check));
    unset($tmp);
    return $str;
}