<?php


//数据库操作类
class db
{
    private $dbdriver;

    // 构造函数，初始化并连接数据库
    public function __construct($cfg=array())
    {
        if(class_exists('PDO') && in_array('mysql',PDO::getAvailableDrivers())){
            require_once(__DIR__.DS.'DBDriver'.DS.'pdo.class.php');
            $this->dbdriver = new db_pdo($cfg);
        }elseif(function_exists('mysqli_connect')){
            require_once(__DIR__.DS.'DBDriver'.DS.'mysqli.class.php');
            $this->dbdriver = new db_mysqli($cfg);
        }else {
            require_once(__DIR__.DS.'DBDriver'.DS.'mysql.class.php');
            $this->dbdriver = new db_mysql($cfg);
        }
    }

    public static function create($cfg){
        $db=new db($cfg);
        return $db->dbdriver;
    }

    public function __get($name){
        if(property_exists($this->dbdriver,$name)){
            return $this->dbdriver->$name;
        }
        return null;
    }

    public function __set($name,$val){
        if(property_exists($this->dbdriver,$name)){
            $this->dbdriver->$name=$val;
        }
    }


    public function __call($name,$args){
        if(method_exists($this->dbdriver,$name)){
            return call_user_func_array(array($this->dbdriver,$name),$args);
        }
    }
}