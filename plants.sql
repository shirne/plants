-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	5.1.73-community

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `plants`
--

DROP TABLE IF EXISTS `plants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `latin_name` varchar(150) DEFAULT NULL,
  `height_min` int(11) DEFAULT NULL,
  `height_max` int(11) DEFAULT NULL,
  `spread_min` int(11) DEFAULT NULL,
  `spread_max` int(11) DEFAULT NULL,
  `temperature_max` int(11) DEFAULT NULL,
  `temperature_min` int(11) DEFAULT NULL,
  `fertilizer` int(11) DEFAULT NULL,
  `sun` int(11) DEFAULT NULL,
  `water` int(11) DEFAULT NULL,
  `taxonomy_group_id` int(11) DEFAULT NULL,
  `is_taxonomy_group_head` tinyint(4) DEFAULT NULL,
  `hidden` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `plants_attributes`
--

DROP TABLE IF EXISTS `plants_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plants_attributes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plant_id` int(11) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `plants_desc`
--

DROP TABLE IF EXISTS `plants_desc`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plants_desc` (
  `plant_id` int(11) NOT NULL,
  `lang` varchar(30) NOT NULL,
  `interesting` text,
  `detail_care` text,
  `harvesting` text,
  `blooming` text,
  `soil_irr` text,
  `growth` text,
  `genus_name` varchar(100) DEFAULT NULL,
  `fertilization` text,
  `description` text,
  `preferred_common_name` varchar(100) DEFAULT NULL,
  `species_name` varchar(100) DEFAULT NULL,
  `subspecies_name` varchar(100) DEFAULT NULL,
  `subspecies_search_ordering` varchar(100) DEFAULT NULL,
  `hardiness_zone_max` varchar(200) DEFAULT NULL,
  `hardiness_zone_min` varchar(200) DEFAULT NULL,
  `description_url` varchar(200) DEFAULT NULL,
  `description_source` varchar(50) DEFAULT NULL,
  `heat_zone_min` varchar(200) DEFAULT NULL,
  `heat_zone_max` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`plant_id`,`lang`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `plants_images`
--

DROP TABLE IF EXISTS `plants_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plants_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plant_id` int(11) DEFAULT NULL,
  `image_path` varchar(200) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `copyright_url` varchar(200) DEFAULT NULL,
  `orig_path` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `plants_names`
--

DROP TABLE IF EXISTS `plants_names`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `plants_names` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plant_id` int(11) NOT NULL,
  `lang` varchar(50) NOT NULL,
  `common_name` varchar(150) DEFAULT NULL,
  `preferred` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-21 16:14:55
