<?php


require_once(__DIR__.DS.'dbbase.class.php');

//数据库操作类
class db_mysqli extends db_base
{


    // 数据库查询(以两维数组[行/列]的形式返回查询的结果)
    // @param string $sql：执行查询的SQL语句
    // @param int $shift：是否将二维数组转成一维数组，1转成一维数组、2不转换，(查询只有一条记录时非常有用)，将只保留二维数组中的第一个数组，移除其余全部
    // @param int $retype：返回结果类型，[0:返回以数字索引方式储存的数组，1:返回以字段名作为键名的数组，2:以数字索引值及以字段名为键值的两种方式同时返回]
    // @return array 返回一个二维或一给数组
    //
    // @example
    // $a = $db->query("SELECT * FROM `mle_admin` LIMIT 0,30",0,1);
    public function query($sql, $shift = 0, $retype = 1)
    {
        $array = array();
        $retype = $retype == 1 ? MYSQLI_ASSOC : ($retype == 0 ? MYSQLI_NUM : MYSQLI_BOTH);
        if ($result = mysqli_query($this->link,$sql)) {
            if($shift) {
                $array=mysqli_fetch_array($result, $retype);
                if($array===false)$array=array();
            }else{
                while ($row = mysqli_fetch_array($result, $retype)) {
                    $array[] = $row;
                }
            }
            mysqli_free_result($result);
            //$shift && is_array($array) && $array = array_shift($array);
        }

        // 调试模式下显示错误信息及 sql 语句
        if (DEBUGGING && $this->link->errno) {
            $this->errorInfo=$this->link->error_list[0];
            die($this->error_msg($sql));
        }
        $this->query_count++; // 查询次数
        return (array)$array;
    }

    public function select($tbl,$where,$fields='*',$order='',$page=-1,$pagesize=20,&$pagecount=0,&$totalcount=0){
        $param=array();
        if(empty($order)){
            $order="`{$this->prikey}` DESC";
        }

        $wherestr=$this->getWhere($where,$param);
        $limit="";
        if($page>=0) {
            if($page==0)$page=1;
            $totalcount = $this->count($tbl, $where);
            $pagecount = ceil($totalcount / $pagesize);
            if ($pagecount < 1) $pagecount = 1;
            if($page>$pagecount)$page=$pagecount;
            $limit=" LIMIT ".(($page-1)*$pagesize).",".$pagesize;
        }
        $sql="SELECT $fields FROM `{$this->prefix}$tbl` $wherestr ORDER BY $order".$limit;

        return $this->execute($sql,$param);
    }

    public function selectOne($tbl,$where,$fields='*',$order=''){
        $param=array();
        if(!empty($order)){
            $order="ORDER BY ".$order;
        }

        $wherestr=$this->getWhere($where,$param);

        $sql="SELECT $fields FROM `{$this->prefix}$tbl` $wherestr $order LIMIT 1";

        $result=$this->execute($sql,$param);
        if(!empty($result)) {
            return $result[0];
        }else{
            return array();
        }
    }

    public function count($tbl,$where,&$param=array()){
        $sql="SELECT COUNT(0) AS COUNT_ROW FROM `{$this->prefix}$tbl` ".$this->getWhere($where,$param);

        $result=$this->execute($sql,$param);
        if(empty($result))return 0;
        return $result[0]['COUNT_ROW'];
    }

//	public static function query_count(){
//		return self::$query_count;
//	}

    public function update($tbl,$data,$where=array()){
        $updatestr=array();
        $params=array();
        //$types='';
        foreach($data as $key=>$d){
            $updatestr[]="`$key`=?";
            $params[]=$d;
            //$types.=$this->getType($d);
        }

        $sql="UPDATE `{$this->prefix}$tbl` SET ".implode(', ',$updatestr);
        if(!empty($wherestr)){
            $sql.= $this->getWhere($where,$params);
        }
        //$stmt=$this->link->prepare($sql);
        //call_user_func_array(array($stmt,'bind_param'),array_merge(array($types),$params));
        return $this->execute($sql,$params);//$stmt->execute();
    }

    public function insert($tbl,$data){
        //$ws=array();
        //$types='';
        /*foreach($data as $key=>$d){
            $ws[]='?';
            $types.=$this->getType($d);
        }*/

        $sql="INSERT INTO `{$this->prefix}$tbl`(`".implode('`, `',array_keys($data))."`) VALUES(".implode(', ',array_fill(0,count($data),'?')).")";
        //$stmt=$this->link->prepare($sql);
        //call_user_func_array(array($stmt,'bind_param'),array_merge(array($types),$data));
        return $this->execute($sql,$data);//$stmt->execute();
    }

    public function exists($tbl,$id,$key=''){
        if(empty($key))$key=$this->prikey;

        $has=$this->selectOne($tbl,array($key=>$id),$key);

        if(empty($has)){
            return false;
        }else{
            return true;
        }
    }

    public function getType($val){
        //忽略 b
        if(is_int($val)){
            return 'i';
        }elseif(is_numeric($val)){
            return 'd';
        }else{
            return 's';
        }
    }

    public function delete($tbl,$where){
        $params=array();
        //$types='';

        $sql="DELETE FROM `{$this->prefix}$tbl` ".$this->getWhere($where,$params);
        //$stmt=$this->link->prepare($sql);
        //call_user_func_array(array($stmt,'bind_param'),array_merge(array($types),$params));
        return $this->execute($sql,$params);//$stmt->execute();
    }

    private function getWhere($where,&$params,$prefix=' WHERE '){
        if($where===''||$where===null||$where===false)return '';
        $wherestr=array();
        $glue=' AND ';
        if(is_array($where)){
            if($this->isValueArray($where)){
                if($this->hasOperator($where[0])) {
                    $wherestr = $where;
                }else {
                    $wherestr[] = "`{$this->prikey}` IN (" . implode(', ', array_map(array($this, 'escape'), $where)) . ")";
                }
            }else {
                foreach ($where as $key => $val) {
                    if (is_array($val)) {
                        if ($this->isValueArray($val)) {
                            $wherestr[] = "`$key` IN (" . implode(', ', array_map(array($this, 'escape'), $val)) . ")";
                        } else {
                            //内嵌where
                            $wherestr[] = '(' . $this->getWhere($val, $params,'') . ')';
                        }
                    } elseif (is_numeric($val) && is_int($key)) {
                        $wherestr[] = "`{$this->prikey}`=" . $this->escape($val);
                    } elseif (!is_numeric($key)) {
                        $wherestr[] = "`$key`=?";
                    } else {
                        if (in_array(strtolower(trim($val)), array('and', 'or'))) {
                            $glue = ' ' . strtoupper($val) . ' ';
                        } else {
                            $wherestr[] = $val;
                        }
                    }
                }
            }
        }elseif(is_numeric($where)){
            $wherestr[]="`{$this->prikey}`=?";
            $params[]=$where;
        }elseif(!empty($where)){
            $wherestr[]=$where;
        }

        return $prefix.implode($glue,$wherestr);
    }

    private function escape($val){
        if(is_int($val))return $val;
        if(is_numeric($val))return (double)$val;
        if(is_bool($val))return $val?1:0;
        return "'".mysqli_real_escape_string($this->link,$val)."'";
    }

    // 执行数据库增、删、改操作
    // @param string $sql：要执行的sql语句
    // @return bool 返回执行结果
    public function execute($sql,$param=array())
    {
        $stat=$this->link->prepare($sql);
        if(!empty($param)){
            $types='';
            foreach($param as $pam){
                $types .= $this->gettype($pam);
            }
            call_user_func_array(array($stat,'bind_param'),array_merge(array($types),$param));
        }
        $stat->execute();
        $result = $stat->get_result();
        if($result && $this->hasDataSql($sql)){
            $result=$stat->fetch_all(MYSQLI_ASSOC);
        }
        $this->affected_rows=$stat->affected_rows;
        //调试模式下显示错误信息及 sql 语句
        if (DEBUGGING && $stat->errno) {
            $this->errorInfo=$stat->error_list[0];
            die($this->error_msg($sql));
        }
        return $result;
    }

    // 获取上一步 INSERT 操作产生的ID
    public function get_last_id()
    {
        return $this->link->insert_id;
    }

    public function affected_rows(){
        return $this->affected_rows;
    }

    // 获取 MySQL 版本
    // @param bool $isformat：是否格式化
    public function version($isformat = true)
    {
        $result = $this->link->server_info;
        if ($isformat) {
            //$result = substr($result,0,strpos($result,'-'));
            $result = explode('-', $result); //substr($mysql_version,0,strpos($mysql_version,'-'));
            $result = $result[0];
        }
        return $result;
    }

    //获得错误描述
    public function get_error()
    {
        return $this->link->error;
    }

    // 打开一个 MySQL 连接
    public function open()
    {
        $host=explode(':',$this->host);
        if(empty($host[1]))$host[1]=3306;
        $this->link=@new mysqli($host[0], $this->user, $this->pass,$this->name,$host[1]);
        if (!$this->link->connect_errno) {
            $this->link->query('set names ' . $this->charset);
            $this->is_open = true;
            return true;
        } else {
            $this->errorInfo=array('errno'=>$this->link->connect_errno,'error'=>$this->link->connect_error);
            die($this->error_msg());
        }
    }

    // 关闭数据库，页面底部调用
    public function close()
    {
        if ($this->is_open) {
            mysqli_close($this->link);
            $this->is_open = false;
        }
        return true;
    }

    // 获取数据表中所有的字段(除了排除的字段以外)
    // @param string $table：表名，不含前缀
    // @param array $remove：排除的字段名称(数组)
    // @return array：返回所有的字段名称
    public function get_fields($table, $remove = array())
    {
        $field = $this->query("SHOW FIELDS FROM `{$this->prefix}{$table}`", 0, 0);
        $result = array();
        foreach ($field as $n) {
            $result[] = $n[0];
        }
        $result = array_values(array_diff($result, $remove));
        return $result;
    }

    // 获取数据库中所有的表(当前前缀名)
    // @return array：返回所有的表名
    public function get_tables()
    {
        $table = $this->query("SHOW TABLES FROM `{$this->name}`", 0, 0);
        $result = array();
        $dbprefix = explode('`.`', $this->prefix);
        $dbprefix = $dbprefix[1];
        foreach ($table as $n) {
            substr($n[0], 0, strlen($dbprefix)) == $dbprefix && $result[] = $n[0];
        }
        return $result;
    }

    // 通过 id 删除一条或多条数据
    // @param string $table 数据表名，不含前缀
    // @param int or array $ids 可以为一个数字或一个数组
    // @return bool
    /*public function delete($table, $ids)
    {
        is_numeric($ids) && $ids = array($ids);
        if (!is_array($ids)) return true;
        $result = $this->execute("DELETE FROM `" . $this->prefix . $table . "` WHERE `id` in ('" . implode("','", $ids) . "');");
        return $result;
    }*/

    // 连接数据库错误信息
    private function error_msg($sql = false)
    {
        $error=$this->errorInfo;
        if(empty($error)){
            if(!empty($this->link->errno))$error=$this->link->error_list[0];
            else $error=array('errno'=>'','error'=>'Unknown error !');
        }
        return parent::showerror($error['errno'],$error['error'], $sql);
    }
}