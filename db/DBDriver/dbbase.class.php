<?php


//数据库操作类
abstract class db_base
{
    // 数据库状态，是否打开
    protected $is_open = false;

    // 数据库连接ID，conn
    protected $link;

    // 字符集
    protected $charset = 'utf8';

    public $host, $user, $pass, $name, $prefix;

    public $query_count = 0; // 数据库查询次数，测试时检测用

    protected $struct;
    protected $prikey='id';

    protected $errorInfo;

    /**
     * @param $DB
     */
    public function __construct($DB)
    {
        $this->host = $DB['host'];
        $this->user = $DB['user'];
        $this->pass = $DB['pass'];
        $this->name = $DB['name'];
        $this->prefix = $DB['prefix'];
        // 打开数据库
        $this->open();
    }

    /**
     * 旧的查询用法 (以两维数组[行/列]的形式返回查询的结果)
     * @param string $sql 执行查询的SQL语句
     * @param int $shift 是否将二维数组转成一维数组，1转成一维数组、2不转换，(查询只有一条记录时非常有用)，将只保留二维数组中的第一个数组，移除其余全部
     * @param int $retype 返回结果类型，[0:返回以数字索引方式储存的数组，1:返回以字段名作为键名的数组，2:以数字索引值及以字段名为键值的两种方式同时返回]
     * @return array 返回一个二维或一给数组
     *
     * @example $a = $db->query("SELECT * FROM `mle_admin` LIMIT 0,30",0,1);
     */
    abstract public function query($sql, $shift = 0, $retype = 1);

    //以下两个为 query替代用法

    /**
     * (分页)查询,可以输出分页数
     * @param string $tbl
     * @param mixed $where 条件，可以为空，可以为数组，可以嵌套
     * @param string $order 排序
     * @param int $page 页码
     * @param int $pagesize 每页大小
     * @param int $pagecount 输出页码总数
     * @param string $fields 查询的字段
     * @param int $totalcount 返回查询总记录数
     * @return array 二维查询结果
     */
    abstract public function select($tbl,$where,$fields='*',$order='',$page=-1,$pagesize=20,&$pagecount=0,&$totalcount=0);

    /**
     * 单条结果查询
     * @param string $tbl
     * @param mixed $where
     * @param string $fields
     * @param string $order
     * @return array 一维数组
     */
    abstract public function selectOne($tbl,$where,$fields='*',$order='');

    /**
     * 查询结果集数目
     * @param $tbl 表名不带前缀
     * @param $where 条件可以为空，可以为数组，可以嵌套
     * @param array $param 内部调用，返回参数集
     * @return int
     */
    abstract public function count($tbl,$where,&$param=array());

    //abstract public static function query_count();

    /**
     * 更新(UPDATE)语句
     * @param $tbl
     * @param $data
     * @param array $where
     * @return bool
     */
    abstract public function update($tbl,$data,$where=array());

    /**
     * 插入(INSERT)语句
     * @param $tbl
     * @param $data
     * @return bool
     */
    abstract public function insert($tbl,$data);

    /**
     * 根据条件查询是否存在结果
     * @param $tbl
     * @param $id
     * @param string $key
     * @return bool
     */
    abstract public function exists($tbl,$id,$key='');

    /**
     * 执行数据库增、删、改、查操作
     * @param $sql 要执行的sql语句
     * @param array $param 查询参数(pdo和mysqli使用系统参数编译语法，mysql可使用类似pdo语法)
     * @return mixed 返回执行结果,如果是select语句,返回结果集
     */
    abstract public function execute($sql,$param=array());

    /**
     * 获取上一步 INSERT 操作产生的ID
     * @return int
     */
    abstract public function get_last_id();

    /**
     * 获取影响的行
     * @return int
     */
    abstract public function affected_rows();

    /**
     * 获取 MySQL 版本
     * @param bool|true $isformat 是否格式化
     * @return string
     */
    abstract public function version($isformat = true);

    /**
     * 获得错误描述
     * @return mixed
     */
    abstract public function get_error();

    /**
     * 打开一个 MySQL 连接
     * @return null
     */
    abstract public function open();

    /**
     * 关闭数据库
     * @return mixed
     */
    abstract public function close();

    /**
     * 获取数据表中所有的字段(除了排除的字段以外)
     * @param $table 表名，不含前缀
     * @param array $remove 排除的字段名称(数组)
     * @return array 返回所有的字段名称
     */
    abstract public function get_fields($table, $remove = array());

    /**
     * 获取数据库中所有的表(当前前缀名)
     * @return array 返回所有的表名
     */
    abstract public function get_tables();

    /**
     * 通过 id 删除一条或多条数据
     * @param $table 数据表名，不含前缀
     * @param mixed $ids 可以为一个数字或一个数组或字符串条件
     * @return mixed
     */
    abstract public function delete($table, $ids);

    //abstract public function delete($tbl,$where);

    protected function hasOperator($str){
        $str = trim($str);
        if ( ! preg_match("/(\s|<|>|!|=|is null|is not null)/i", $str))
        {
            return FALSE;
        }
        return TRUE;
    }

    /**
     * 检测数组是否数字索引，用于主键或相关条件
     * @param $arr
     * @return bool
     */
    protected function isValueArray($arr){
        return array_keys($arr) === range(0, count($arr) - 1);
    }

    /**
     * 判断sql语句的查询类型
     * @param $sql
     * @return string 大写(SELECT,UPDATE,INSERT,DELETE)
     */
    protected function sqlType($sql){
        $sql=trim($sql);
        $match=array();
        if(preg_match('#^[a-zA-Z]+#',$sql,$match)){
            return strtoupper($match[0]);
        }
        return 'UNKNOWN';
    }

    protected function hasDataSql($sql){
        return in_array($this->sqlType($sql),array('SELECT','SHOW','DESCRIBE'));
    }

    /**
     * 连接数据库错误信息
     * @param $errno
     * @param $errmsg
     * @param bool|false $sql
     * @return string
     */
    protected function showerror($errno,$errmsg,$sql = false)
    {
        $Msg = '<span style="font-size:12px; font-family:verdana,lucida;">';
        $Msg .= '<b>Time : </b>' . date("Y-m-d H:i:s") . '<br />';
        $Msg .= '<b>Script : </b>' . $_SERVER['PHP_SELF'] . '<br /><br />';
        $Msg .= '<b>Error : </b>' . $errmsg . '<br />';
        $Msg .= '<b>Errno : </b>' . $errno . '<br /><br />';
        $sql && $Msg .= '<b>SQL : </b>' . $sql . '<br /><br />';
        $Msg .= 'Similar error report has been dispatched to administrator before.<br />';
        $Msg .= '</span>';
        return $Msg;
    }
}