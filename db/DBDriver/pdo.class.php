<?php


require_once(__DIR__.DS.'dbbase.class.php');

//数据库操作类
class db_pdo extends db_base
{
    private $affected_rows;

    // 数据库查询(以两维数组[行/列]的形式返回查询的结果)
    // @param string $sql：执行查询的SQL语句
    // @param int $shift：是否将二维数组转成一维数组，1转成一维数组、2不转换，(查询只有一条记录时非常有用)，将只保留二维数组中的第一个数组，移除其余全部
    // @param int $retype：返回结果类型，[0:返回以数字索引方式储存的数组，1:返回以字段名作为键名的数组，2:以数字索引值及以字段名为键值的两种方式同时返回]
    // @return array 返回一个二维或一给数组
    //
    // @example
    // $a = $db->query("SELECT * FROM `mle_admin` LIMIT 0,30",0,1);
    public function query($sql, $shift = 0, $retype = 1)
    {
        $stmt=$this->link->prepare($sql);
        $array = array();
        $retype = $retype == 1 ? PDO::FETCH_ASSOC : ($retype == 0 ? PDO::FETCH_NUM : PDO::FETCH_BOTH);
        if ($result = $stmt->execute()) {
            if($shift){
                $array=$stmt->fetch($retype);
                if($array===false)$array=array();
            }else {
                $array = $stmt->fetchAll($retype);
            }
            //$shift && is_array($array) && $array = array_shift($array);

        }
        $this->errorInfo=$stmt->errorInfo();
        $stmt->closeCursor();
        // 调试模式下显示错误信息及 sql 语句
        if (DEBUGGING && $this->hasError()) {
            die($this->error_msg($sql));
        }
        $this->query_count++; // 查询次数
        return (array)$array;
    }

    public function select($tbl,$where,$fields='*',$order='',$page=-1,$pagesize=20,&$pagecount=0,&$totalcount=0){
        $param=array();
        if(empty($order)){
            $order="`{$this->prikey}` DESC";
        }

        $wherestr=$this->getWhere($where,$param);
        $limit="";
        if($page>=0) {
            if($page==0)$page=1;
            $totalcount = $this->count($tbl, $where);
            $pagecount = ceil($totalcount / $pagesize);
            if ($pagecount < 1) $pagecount = 1;
            if($page>$pagecount)$page=$pagecount;
            $limit=" LIMIT ".(($page-1)*$pagesize).",".$pagesize;
        }
        $sql="SELECT $fields FROM `{$this->prefix}$tbl` $wherestr ORDER BY $order".$limit;

        return $this->execute($sql,$param);
    }

    public function selectOne($tbl,$where,$fields='*',$order=''){
        $param=array();
        if(!empty($order)){
            $order="ORDER BY ".$order;
        }

        $wherestr=$this->getWhere($where,$param);

        $sql="SELECT $fields FROM `{$this->prefix}$tbl` $wherestr $order LIMIT 1";
        $result=$this->execute($sql,$param);

        if(!empty($result)) {
            return $result[0];
        }else{
            return array();
        }
    }

    public function count($tbl,$where,&$param=array()){
        $sql="SELECT COUNT(0) AS COUNT_ROW FROM `{$this->prefix}$tbl` ".$this->getWhere($where,$param);

        $result=$this->execute($sql,$param);
        if(empty($result))return 0;
        return $result[0]['COUNT_ROW'];
    }

//	public static function query_count(){
//		return self::$query_count;
//	}

    public function update($tbl,$data,$where=array()){
        $updatestr=array();
        $params=array();
        foreach($data as $k=>$d){
            if(is_numeric($k)) {
                $updatestr[]=$d;
            }elseif(strpos($k,' ')>0){
                $updatestr[]=$k;
            }else {
                $updatestr[] = "`$k`=:$k";
                $params[":$k"] = $d;
            }
        }

        $sql="UPDATE `{$this->prefix}$tbl` SET ".implode(', ',$updatestr);
        if(!empty($where)){
            $sql .= $this->getWhere($where,$params);
        }
        //$stmt=$this->link->prepare($sql);
        return $this->execute($sql,$params);//$stmt->execute($param);
    }

    public function insert($tbl,$data){
        $keys=array_keys($data);
        $params=array();
        foreach($data as $k=>$v){
            $params[":$k"]=$v;
        }
        $sql="INSERT INTO `{$this->prefix}$tbl`(`".implode('`, `',$keys)."`) VALUES(:".implode(', :',$keys).")";
        //$stmt=$this->link->prepare($sql);
        return $this->execute($sql,$params);//$stmt->execute($params);
    }

    public function exists($tbl,$id,$key=''){
        if(empty($key))$key=$this->prikey;

        $has=$this->selectOne($tbl,array($key=>$id),$key);

        if(empty($has)){
            return false;
        }else{
            return true;
        }
    }

    public function delete($tbl,$where){
        $params=array();

        if(empty($where))throw new Exception('删除操作必须指定条件！');

        $sql="DELETE FROM `{$this->prefix}$tbl` ".$this->getWhere($where,$params);
        //$stmt=$this->link->prepare($sql);
        return $this->execute($sql,$params);//$stmt->execute($param);
    }

    private function getWhere($where,&$params=array(),$prefix=' WHERE '){
        if($where===''||$where===null||$where===false)return '';
        $wherestr=array();
        $glue=' AND ';
        if(is_array($where)) {
            if($this->isValueArray($where)){
                if(count($where)<1)return '';
                if($this->hasOperator($where[0])) {
                    $wherestr = $where;
                }else{
                    $i = 1;
                    $pk = $this->prikey;
                    //防止键重复
                    while (isset($params[":{$pk}_0"])) {
                        $pk = $this->prikey . $i;
                        $i++;
                    }
                    $keys = array();
                    foreach ($where as $q => $v) {
                        $keys[] = ":{$pk}_{$q}";
                        $params[":{$pk}_{$q}"] = $v;
                    }
                    $wherestr[] = "`{$this->prikey}` IN (" . implode(', ', $keys) . ")";
                }
            }else {
                foreach ($where as $k => $d) {
                    if (is_array($d)) {
                        if ($this->isValueArray($d)) {
                            $i = 1;
                            $pk = $k;
                            //防止键重复
                            while (isset($params[":{$pk}_0"])) {
                                $pk = $k . $i;
                                $i++;
                            }
                            $keys = array();
                            foreach ($d as $q => $v) {
                                $keys[] = ":{$pk}_{$q}";
                                $params[":{$pk}_{$q}"] = $v;
                            }
                            $wherestr[] = "`$k` IN (" . implode(', ', $keys) . ")";
                        } else {
                            $wherestr[] = '(' . $this->getWhere($d, $params,'') . ')';
                        }
                    } elseif (!is_numeric($k)) {
                        $i = 1;
                        $pk = $k;
                        //防止键重复
                        while (isset($params[":$pk"])) {
                            $pk = $k . $i;
                            $i++;
                        }
                        $wherestr[] = "`$k`=:$pk";
                        $params[":$pk"] = $d;
                    } else {
                        if (in_array(strtolower(trim($d)), array('and', 'or'))) {
                            $glue = ' ' . strtolower(trim($d)) . ' ';
                        } else {
                            $wherestr[] = $d;
                        }
                    }
                }
            }
        }elseif(is_numeric($where)){
            $i = 1;
            $pk = $this->prikey;
            while (isset($params[":$pk"])) {
                $pk = $this->prikey . $i;
                $i++;
            }
            $wherestr[]="`{$this->prikey}`=:$pk";
            $params[":$pk"]=$where;
        }elseif(!empty($where)){
            $wherestr[]=$where;
        }
        return $prefix.implode($glue,$wherestr);
    }

    // 执行数据库增、删、改操作
    // @param string $sql：要执行的sql语句
    // @return bool 返回执行结果
    public function execute($sql,$param=array())
    {
        $stmt=$this->link->prepare($sql);
        $result = $stmt->execute($param);

        if($result && $this->hasDataSql($sql)){
            $result=$stmt->fetchAll(PDO::FETCH_ASSOC);
        }
        $this->affected_rows=$stmt->rowCount();
        $this->errorInfo=$stmt->errorInfo();
        $stmt->closeCursor();
        //调试模式下显示错误信息及 sql 语句
        if (DEBUGGING && $this->hasError()) {
            die($this->error_msg($sql));
        }
        return $result;
    }

    // 获取上一步 INSERT 操作产生的ID
    public function get_last_id()
    {
        return $this->link->lastInsertId();
    }

    public function affected_rows(){
        return $this->affected_rows;
    }

    // 获取 MySQL 版本
    // @param bool $isformat：是否格式化
    public function version($isformat = true)
    {
        $result = $this->link->getAttribute(PDO::ATTR_SERVER_VERSION);
        if ($isformat) {
            $result = explode('-', $result);
            $result = $result[0];
        }
        return $result;
    }

    //获得错误描述
    public function get_error()
    {
        return $this->errorInfo;
    }

    // 打开一个 MySQL 连接
    public function open()
    {
        $hosts=explode(':',$this->host);
        $dsn="mysql:dbname={$this->name};host={$hosts[0]}";
        if(!empty($hosts[1]))$dsn .= ";port={$hosts[1]}";

        try {
            $this->link = new PDO($dsn, $this->user, $this->pass);
            $res=$this->link->exec('set names ' . $this->charset);
            $this->is_open = true;
            return true;
        }catch (PDOException $e){
            $this->errorInfo=array($e->getCode(),$e->getMessage());
            die($this->error_msg());
        }
    }

    // 关闭数据库，页面底部调用
    public function close()
    {
        if ($this->is_open) {
            //$this->link->close();
            $this->link=null;
            $this->is_open = false;
        }
        return true;
    }

    // 获取数据表中所有的字段(除了排除的字段以外)
    // @param string $table：表名，不含前缀
    // @param array $remove：排除的字段名称(数组)
    // @return array：返回所有的字段名称
    public function get_fields($table, $remove = array())
    {
        $field = $this->query("SHOW FIELDS FROM `{$this->prefix}{$table}`", 0, 0);
        $result = array();
        foreach ($field as $n) {
            $result[] = $n[0];
        }
        $result = array_values(array_diff($result, $remove));
        return $result;
    }

    // 获取数据库中所有的表(当前前缀名)
    // @return array：返回所有的表名
    public function get_tables()
    {
        $table = $this->query("SHOW TABLES FROM `{$this->name}`", 0, 0);
        $result = array();
        $dbprefix = explode('`.`', $this->prefix);
        $dbprefix = $dbprefix[1];
        foreach ($table as $n) {
            substr($n[0], 0, strlen($dbprefix)) == $dbprefix && $result[] = $n[0];
        }
        return $result;
    }

    // 通过 id 删除一条或多条数据
    // @param string $table 数据表名，不含前缀
    // @param int or array $ids 可以为一个数字或一个数组
    // @return bool
    /*public function delete($table, $ids)
    {
        is_numeric($ids) && $ids = array($ids);
        if (!is_array($ids)) return true;
        $result = $this->execute("DELETE FROM `" . $this->prefix . $table . "` WHERE `id` in ('" . implode("','", $ids) . "');");
        return $result;
    }*/

    private function hasError(){
        if(!empty($this->errorInfo))$errcode=$this->errorInfo[0];
        return !(empty($errcode) || $errcode=='00000');
    }

    // 连接数据库错误信息
    private function error_msg($sql = false)
    {
        $error = $this->errorInfo;
        if(empty($error)) {
            $error = array("","-1", "Unknown error !");
        }
        return parent::showerror($error[1], $error[2], $sql);
    }
}