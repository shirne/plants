<?php
/**
 * 保存数据到mysql中
 * 可用参数:
 *  1->当前语言 ch/en
 */



//函数库
require('common.php');
require('db/db.class.php');


if(isset($argv[1]) && !empty($argv[1])){
    $lang=trim($argv[1]);
}
$mode="";
if(!empty($argv[2])){
    $mode=trim($argv[2]);
}

$db=db::create(array(
    'host'=>'localhost',
    'user'=>'root',
    'pass'=>'123456',
    'name'=>'test',
    'prefix'=>''
    ));


$ch = curl_init();

//curl_setopt( $ch, CURLOPT_HTTP_VERSION , CURL_HTTP_VERSION_1_1 );
curl_setopt( $ch, CURLOPT_HEADER, 1);
curl_setopt( $ch, CURLOPT_USERAGENT , 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)' );
curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT , 60 );
curl_setopt( $ch, CURLOPT_TIMEOUT , 60);
curl_setopt( $ch, CURLOPT_RETURNTRANSFER , true );
curl_setopt( $ch, CURLOPT_AUTOREFERER, true );
curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true); 

curl_setopt( $ch, CURLOPT_COOKIESESSION, TRUE); 
//curl_setopt( $ch, CURLOPT_COOKIEFILE, $cookie);
//curl_setopt( $ch, CURLOPT_COOKIEJAR, $cookie);
//curl_setopt( $ch, CURLOPT_COOKIE, session_name() . '=' . session_id()); 
//curl_setopt( $ch, CURLOPT_COOKIE, GetCookies($cookie));

curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, FALSE);
curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, FALSE);

curl_setopt( $ch, CURLOPT_URL , $rooturl);
$data=curl_exec($ch);
$data=ihttp_response_parse($data);


//授权
checkauth();

//en_us zh-CN zh
$idindex=getdata($rooturl.$url_index);

$headers=false;


if(!empty($idindex['found'])){
	//获取已导入的
	$loaded=array();
    if($mode!='update'){
    	$lists=$db->select('plants_desc',array('lang'=>$lang),'plant_id','plant_id ASC');
    	if(!empty($lists)){
    		$loaded=array_column($lists,'plant_id');
    	}
    }

    $request=array();
    sort($idindex['found']);
    foreach($idindex['found'] as $id){
    	if(in_array($id,$loaded))continue;
        $request[]=$id;
        //批量获取
        if(count($request)>19){
            checkauth();
            $datas=getdata($rooturl.$url_list.implode(',', $request));
            if(!empty($datas) && !empty($datas['plants'])){
            	
                foreach ($datas['plants'] as $plant) {

                    $plants=array();
                    $desc=array('plant_id'=>$plant['id'],'lang'=>$lang);
                    $attributes=array();
                    $images=array();
                    $names=array();
                    foreach ($plant as $key => $value) {
                        if($key=='attributes'){
                            foreach ($value as $k=>$attr) {
                                foreach ($attr as $val) {
                                    $attributes[]=array('plant_id'=>$plant['id'],'name'=>$k,'value'=>$val);
                                }
                            }
                        }elseif($key=='common_names'){
                            foreach ($value as $name) {
                                $name['lang']=$lang;
                                $name['plant_id']=$plant['id'];
                                $names[]=$name;
                            }
                        }elseif($key=='images'){
                            foreach ($value as $image) {
                                $filename=file_name($image['image_path']);
                                $images[]=array(
                                    'plant_id'=>$plant['id'],
                                    'image_path'=>($plant['id']%100).'/'.$plant['id'].'/'.$filename,
                                    'orig_path'=>$image['image_path'],
                                    'name'=>$image['copyright']['name'],
                                    'copyright_url'=>$image['copyright']['copyright_url']
                                    );
                            }
                        }elseif(in_array($key,array("id","latin_name","height_min","height_max","spread_min","spread_max","temperature_max","temperature_min","fertilizer","sun","water","taxonomy_group_id","is_taxonomy_group_head","hidden"))){
                            $plants[$key]=$value;
                        }elseif(in_array($key,array("interesting","detail_care","harvesting","blooming","soil_irr","growth","genus_name","fertilization","description","preferred_common_name","species_name","subspecies_name","subspecies_search_ordering","hardiness_zone_max","hardiness_zone_min","description_url","description_source","heat_zone_min","heat_zone_max"))){
                            $desc[$key]=$value;
                        }
                    }
                    if(!$db->exists('plants',$plant['id'])){
                        $db->insert('plants',$plants);
                        foreach ($attributes as $attribute) {
                            $db->insert('plants_attributes',$attribute);
                        }
                        foreach ($images as $image) {
                            $db->insert('plants_images',$image);
                        }
                    }
                    $db->insert('plants_desc',$desc);
                    foreach ($names as $name) {
                        $db->insert('plants_names',$name);
                    }
                }

            	echo "Loaded:".implode(',', $request)."\n";
            }
            
            sleep(1);
            $request=array();
        }
    }
}

curl_close( $ch );
