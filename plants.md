#UA
	Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.93 Safari/537.36

#Refer
https://myflowerpower.parrot.com/

#授权
https://myflowerpower.parrot.com/user/v1/authenticate
##参数
	grant_type:client_credentials
	client_id:web_access_001
	client_secret:6c2a6a924c385e72c6bb824c3a98c3d28719cfcf7b66
##返回
	{
	"access_token":"EiuQ61ji6163FnUMefqJIp54AaWrRAHL8Dvifo7e8mGFZTYZ",
	"expires_in":86400
	}

#索引

https://myflowerpower.parrot.com/search/v5/plants/?generate_index=ASC

##headers
	X-Rack-Cache:miss
	X-Request-Id:600ffc6f-7c4f-40c4-9c0f-8c187a359741
	X-Runtime:1.536075
	X-Ua-Compatible:IE=Edge,chrome=1

#cookie
guest_access_token=nZIovQ46iEPUATJ9OAyLYAsvOGrXHVka1w34orRAibm6Zryd; userLang=zh; __utmt=1; __utma=145429416.586764396.1445225028.1445225028.1445225028.1; __utmb=145429416.8.10.1445225028; __utmc=145429416; __utmz=145429416.144522502

	{
		alpha_index: {a: 0, b: 825, c: 1097, d: 2261, e: 2652, f: 2916, g: 3055, h: 3263, i: 3761, j: 4216, k: 4285,…}
		errors: []
		found: [5642, 9272, 3, 6572, 6573, 6574, 6575, 9271, 99999, 1, 2, 5520, 5523, 5522, 8085, 2697, 2694, 2695,…]
		found_count: 8076
		server_identifier: "1.1.12(p0) January 15th 2015"
	}

#摘要
https://myflowerpower.parrot.com/plant_library/v1/plants/5642,9272,3,6572,6573,6574,6575,9271,99999,1,2,5520,5523,5522,8085,2697,2694,2695,2693,2696

##headers
	X-Rack-Cache:miss
	X-Request-Id:58fce9a5-3081-4b37-8b79-f679dadbb347
	X-Runtime:0.614385
	X-Ua-Compatible:IE=Edge,chrome=1

##返回
	{
		errors: []
		plants: [{
			attributes: {BL: ["PI", "WH", "PU"], FO: ["GR", "BR"], PT: ["PE", "SH"], SF: ["DR"], SH: ["SP"],…}
			blooming: ""
			common_names: [{common_name: "糯米条", preferred: true}]
			description: ""
			description_source: "easybloom"
			description_url: null
			detail_care: null
			fertilization: null
			fertilizer: 1
			fullname: "Abelia chinensis"
			genus_name: "六道木属"
			growth: ""
			hardiness_zone_max: ""
			hardiness_zone_min: "在年平均最低温度为-12°C (10 °F)以下的地区，这种植物可能会受到无法挽回的损伤。"
			harvesting: null
			heat_zone_max: "在150天极炎热的光照条件下（平均温度高于30℃/ 86℉），这种植物可能会受到无法挽回的损伤。"
			heat_zone_min: "在61天以上的极炎热的光照条件下（平均温度高于30℃/ 86℉），这种植物可能会受到损伤。"
			height_max: 185
			height_min: 150
			hidden: false
			id: 5642
			images: [{image_path: "https://s3.amazonaws.com/dev-plant-library/TRE14525_GWI.jpg",…},…]
			interesting: ""
			is_taxonomy_group_head: false
			latin_name: "Abelia chinensis"
			pests: "没有严重的病或虫害问题。"
			planting: ""
			preferred_common_name: "糯米条"
			pruning: ""
			soil_irr: ""
			species_name: "糯米条"
			spread_max: 295
			spread_min: 195
			subspecies_name: null
			subspecies_search_ordering: "00000"
			sun: 3
			taxonomy_group_id: 1358
			temperature_max: 40
			temperature_min: 7
			water: 2
		}]
		server_identifier: "1.1.12(p0) January 15th 2015"
	}

#详细
https://myflowerpower.parrot.com/plant_library/v2/plant/9272

##返回
	{
		errors:[],
		response:{
			attributes:{},
			blooming:'',
			common_names:[{}],
			description:'',
			description_source:'',
			description_url:'',
			detail_care:'',
			fertilization:'',
			fertilizer:'',
			fullname:'',
			genus_name: ""
			growth: ""
			hardiness_zone_max: null
			hardiness_zone_min: null
			harvesting: null
			heat_zone_max: null
			heat_zone_min: null
			height_max: 120
			height_min: 90
			hidden: false
			id: 9272
			images: [{image_path: "https://s3.amazonaws.com/dev-plant-library/3432-7_MINIER.jpg",…}]
			0: {image_path: "https://s3.amazonaws.com/dev-plant-library/3432-7_MINIER.jpg",…}
			interesting: ""
			is_taxonomy_group_head: false
			latin_name: "Abelia dielsii"
			pests: ""
			planting: ""
			preferred_common_name: ""
			pruning: ""
			soil_irr: ""
			species_name: ""
			spread_max: 120
			spread_min: 90
			subspecies_name: null
			subspecies_search_ordering: "00000"
			sun: 3
			taxonomy_group_id: 3344
			temperature_max: 40
			temperature_min: 7
			water: 2
		},
		server_identifier:"1.1.12(p0) January 15th 2015"
	}

#属性说明
##TYPE(PT)
-GR Grass
-IP Interior plant
-SH Shrub
-TR Tree
-VI Vine
##特性(SF)
-ED Edible
-DR
-AF

##PLANT SHAPE(SH)植物形状
-IR Irregular
-PY Pyramidal
-RO Rounded
-SP Spreading
-UP Upright

##BLOOM COLOR(BL)花朵颜色
-BK black
-BL blue
-GR green
-OR orange
-PI pink
-PU purple
-RE red
-WH white
-YE yellow

##LEAF COLOR(FO)叶子颜色
-BK black
-BL blue
-BR brown
-GR green
-OR orange
-PI pink
-PU purple
-RE red
-SI silver
-VA variegated
-WH white
-YE yellow

##LIFETIME(PT)周期
-AN Annual
-BA Biennial
-PE Perennial
-SH

##BLOOM SEASON(SN)花期
-S Spring
-E Summer
-F Fall
-W Winter

#相似网站
http://www.plantdb.co.uk/